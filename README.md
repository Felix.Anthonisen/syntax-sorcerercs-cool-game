**General info**

- Gruppe: SyntaxSorcerers (Gruppe 5) - Mathias Angeltvedt, Felix Anthonisen, Malin Volle, Cathrine Trøen, Johanne Nerland
- Link; https://git.app.uib.no/Felix.Anthonisen/syntax-sorcerercs-cool-game

**Project info**

- **`Om spillet:`**

  - Spillet vårt er inspirert av Celeste, et form for explorerer-spill hvor man skal hoppe seg gjennom utfordrende baner, samtidig som man skal finne collectibles rundt om på kartet. I spillet kan man plukke opp forskjellige power-ups som gjør at man kan f.eks. hoppe høyere, dashe to ganger og ha lavere tyngdekraft.
    Du styrer spilleren med `WASD`, hopper med spacebar og dasher med `I`. Man bruker power-ups med `J, K og L`.
    Vi har egentegnet grafikk, som innebærer bakgrunn, figurer, platformer osv. for å gjøre spillet mer livlig for de som skal spille det.

- **`Kjøring av spillet:`**

  - Kompileres med `mvn package`.
  - Kjøres med `java -jar gjeldende path til SNAPSHOT-fil`
  - Eksempel på kjøring: `java -jar target/syntax-sorcerercs-cool-game 1.0-SNAPSHOT-fat.jar`
  - Krever Java 21 eller senere

  - Kan alternativt kjøres med `mvn exec:java`

- **`Kjente feil:`**

  - Spilleren kan sporadisk stå på veggene.

- **`Credits:`**
  - All kode er laget copyright 2024 syntaxsorcerers. Se lisens for mer informasjon.
  - Inspirasjon er tatt fra Celeste: https://store.steampowered.com/app/504230/Celeste/
  - Spillets tilemap "Dungeon Platformer Tileset" er laget av RottingPixels. Noen tiles har blitt lagt til av oss. Link: https://rottingpixels.itch.io/platformer-dungeon-tileset
  - Lydeffekt for når spiller endrer farge "Pick-up Health" laget av juancamiloorjuela er lisensiert under CC0 1.0 DEED. Link: https://freesound.org/people/juancamiloorjuela/sounds/204318/
  - Lydeffekt når spiller plukker opp mynter "CollectCoin.wav" laget av bradwesson er lisensiert under CC BY-NC 4.0 DEED. Link: https://freesound.org/people/bradwesson/sounds/135936/
  - Bakgrunn til spillet er laget av asecas0. Link: https://wallpapers.com/wallpapers/dark-pixel-d86vpb924ivkc68j.html
  - Bakgrunnsmusikk "Cruising Down 8bit Lane" laget av Monument_Music er lisensiert under Pixabay Content License. Link til sangen: https://pixabay.com/music/video-games-cruising-down-8bit-lane-159615/
  - Lydereffekter for å dashe, hoppe, plukke opp powerups og når spilleren dør er laget av Magnus Haaland.
  - Resten av assets til spillet er laget av oss. Dette inkluderer spilleranimasjoner, hovedmeny til spillet og knapper.
