
- **Brukerhistorie klatre på vegg**
Brukerhistorie:
- Som spiller ønsker jeg å kunne klatre på vegger for å nå mål. 

Akseptansekriterier for å klatre på vegg:
- Når spilleren går mot veggen vil jeg at spilleren kan kunne gå opp å ned ved å bruke "W" og "S" og "K" som er selve klatre funksjonen, for å så komme seg opp veggen og derreter kunne nå målet.  

Arbeidsoppgave:
- For å gjennomføre dette må en klatrefunskjon til. Lager en kode for klatring: når hit-boxen til spilleren treffer veggen, blir setCanClimb funksjonen brukt og isClimbing blir satt til true. Ved da å trykke på "K" vil spilleren kunne klatre opp og ned. 

- **Brukerhistorie for å dashe**
Brukerhistorie:
- Som spiller vil jeg kunne dashe/ "fly" bortover på mappet i alle himmelretningene for å lettere komme meg bortover og over hinder. 

Akseptansekriterier for å dashe:
- Når spilleren går bortover vil jeg at spilleren kan kunne fly bortover ved å trykke på tasten "J" og deretter flytter spilleren seg flere pixler bortover, isteden for å bare gå vanlig. 

Arbeidsoppgave:
- Bruker en dashfunksjon for å nå dette. På samme måte som klatring, må vi sette at vi kan dashe til true og derretter velge hvilken retning den skal dashe. Dette er enten nord, sør, vest, øst, nordøst, nordvest, sørøst og sørvest. Når tasten "J" blir trykt skal spilleren dermed kunne fly den retningen "J" tasten blir kombinert med sammen med en av retningstastene (W,A,S eller D), og dermed ha forflyttet seg den gitte retningen. 

- **Brukerhistorie for å hoppe**
Brukerhistorie:
- Som spiller vil jeg kunne forflytte meg opp og ned enkelt ved å klikke på en tast som gjør dette mulig.

Akseptansekriterier for å hoppe:
- For at spilleren skal kunne hoppe opp og ned over hindringer, må space-tasten bli trykket på. 

Arbeidsoppgave:
- I koden for å kunne hoppe setter vi først at spilleren ikke kan klatre lengre, ettersom spilleren ikke både kan klatre og hoppe samtidig. Når klatring er false, har vi en velosity funksjon som gjør at spilleren går oppover ved hjelp av en timer, og dermed kan hoppe opp. 

- **Brukerhistorie for å samle coins**
Brukerhistorie:
- Som spiller vil jeg kunne samle coins for å få mest mulig penger. I tillegg vil spilleren kunne skifte farge etter hvor mange coins man får. 

Akseptansekriterier for å samle coins:
- For å kunne samle coins må spilleren bevege seg i forskjellige maps og gå inn i disse rosa coinsene for å samle de. Det vil da stå i hjørnet til høyre hvor mange man han samlet. Dette skaper motivasjon for å gjennomføre forskjellige baner ettersom at det er forskjellig mengde coins ut ifra hvilken bane man spiller på. 

Arbeidsoppgave:
- Må da ha en kode som gjør at en coin "tile" dukker opp på skjermen. Når spilleren går inn i coinen skal spilleren "collecte" denne og dermed forsvinner coinen fra skjermen slik at man ikke bare kan hente uendelige mange coins. Selve coinen blir da "slettet" fra skjermen. Videre oppdateres en counter på hvor mange coins spilleren har samlet inn. 

- **Brukerhistorie for å skifte farge på spiller**
Brukerhistorie:
- Som spiller vil jeg kunne endre farge på karakteren min underveis i spillet. Det skjer ved å ha samlet en gitt mengde coins. 

Akseptansekriterier for å skifte farge på spiller:
- For å kunne endre farge må spilleren samle nok coins til at karakteren bytter farge. Det er motivasjon å kunne bytte farge for dette skaper variasjon i spillet og man vil derfor ville prøve å komme lengre i spillet for å kunne oppnå denne forandringen.

Arbeidsoppgave:
- For at fargen skal kunne endres er det lagd en kode som jobber ut ifra koden som teller hvor mange coins man har fått. Etter man har samlet inn en viss mengde coins vil karakteren skifte farge ved å gi den en ny sprite som inneholder et bilde med karakteren i en annen farge. Dette skjer etter å ha samlet 10, 50 og 100 coins. 

- **Brukerhistorie for hindringer(spikes)**
Brukerhistorie:
- Som spiller vil jeg ha hindringer for å gjøre spillet til en utfordring. Dette gjør at spillet blir mer interessant ettersom at man ville kunne få en mestringsfølelse av å få til ulike type hindringer i forskjellige vanskelighetsgrader. 

Akseptansekriterier for hindringer:
- Hindringene i dette spillet er hovedsakelig spikes/piggene. Disse blir brukt slik at hvis spilleren treffer på de så dør spilleren og må starte på nytt på den nåværende banen spilleren befinner seg på. Dette gir spilleren utfordringer underveis i spillet. 

Arbeidsoppgave:
- For at spilleren skal dø av hindringene og komme tilbake på mappet har vi lagt ut disse piggene utover på mappet. Når spilleren treffer denne type "tile" vil en respawn funksjon tas i bruk ettersom den får beskjed om at karakteren er død. Da setter vi respawn posisjonen til starten av det mappet spilleren befant seg på og spilleren må prøve på nytt. I tillegg har vi en "death counter" funskjon i venstre hjørnet som vil oppdatere seg for hver gang spilleren dør, og spilleren kan derfor ha oversikt over hvor mange ganger han har dødd. 

- **Brukerhistorie for powerup**
Brukerhistorie:
- Som spiller vil jeg kunne ha en powerup som vil hjelpe meg til å komme over hinder som er spesielt vanskelige, slik at jeg kan få de til på en mer kreativ måte. 

Akseptansekriterier for powerup:
- En flaske vil dukke opp på map som er spesielt vanskelige og denne flasken er en "powerup" som vil gjøre slik at når spilleren treffer på denne, så har spilleren mulighet til å double-dashe, altså dashe en ekstra gang. Spilleren kan vanligvis bare dashe en gang om gangen, og derfor vil denne flasken gjøre slik at hindringene kan bli litt lettere å passere.  

Arbeidsoppgave:
- Har her en kode for å gjøre slik at når spilleren treffer på denne powerup-flasken, så vil dashe funksjonen resete, slik at det er lovlig å dashe igjen (setter dash til true). Derreter blir den gjort om til false, siden den kan maks dashe en gang ekstra. Powerup-flasken har en reload funksjon, slik at det er mulig å prøve på nytt igjen etterhvert, hvis man ikke fikk til å bruke double dash skikkelig på første forsøk. 

- **Brukerhistorie for pause knapp**
Brukerhistorie:
- Som spiller vil jeg kunne trykke på pause underveis i spillet, slik at spillet slipper å stå på i bakgrunnen mens jeg trenger å gjøre noe annet. Da kan jeg komme tilbake til spillet når jeg vil fortsette, uten å måtte starte helt på nytt. 

Akseptansekriterier for pause:
- Når spilleren trykker på "esc" tasten, skal en pause-meny komme opp på skjermen. Den inneholder en hjem knapp slik at man gå til hoved-meny, en restart knapp slik at man kan starte på nytt og en fortsettelse knapp slik at man kan fortsette der man var. 

Arbeidsoppgave:
- Her er en pause skjerm knyttet til "esc" knappen og de forskjellige knappene på pause skjermen endrer gameState til spillet. Fortsettelse gir GameState:running. Starte spillet på nytt: GameState:restart. Gå tilbake til hovedmeny: GameState:mainMenu. 

- **Brukerhistorie for info-skjerm**
Brukerhistorie:
- Som spiller vil jeg kunne ha en info skjerm sånn at jeg ser hva som er meningen med spillet og får en forståelse av hva jeg skal gjøre som spillbruker i et helt nytt spill.

Akseptansekriterier for info-skjerm:
- Når spilleren åpner spillet vil han komme til hovedmenyen. Her er det en info knapp spilleren kan trykke på for å bli videresendt til info-skjermen. Her står det en oversikt over hva spillet går ut på, hvilke type hinder han kan møte på og hva som er meningen med spillet. 

Arbeidsoppgave:
- Det blir lagd en egen GameState som er knyttet til info (GameState:info) som setter spillet til en info skjerm. Hvis GameState er satt til info skjermen vil spillet lage en egen skjerm med alt av tekst og info for selve spillet med en annen lay-out enn for eksempel hovedmenyen. 

- **Brukerhistorie for controls-skjerm**
Brukerhistorie:
- Som spiller vil jeg kunne ha en skjerm som har oversikt over hvilke taster/kontroller jeg skal trykke på for å kunne vite hvordan jeg skal spille spillet hensiktsmessig og utnytte spillets funksjonalitet. 

Akseptansekriterier for controls-skjerm:
- Når spilleren befinner seg i hovedmenyen vil det være en egen knapp for "Controls". Når spilleren trykker inn her, oppstår det et nytt vindu som viser til bilder av hvilke taster som gjør de korresponderende bevegelsene. 

Arbeidsoppgave:
- Samme som med info-skjermen, blir det lagd en GameState som er knyttet til controls som setter spillet til en controls skjerm. Her blir det lagd en egen skjerm med implementasjon av kontrollene som brukes og beskrivelse til disse. 

- **Brukerhistorie for design**
Brukerhistorie:
- Som en spiller vil jeg at spillet skal se fint ut og ha fine farger for å vekke interesse og nysgjerrighet. Jeg vil at desginet skal se fint og enkelt ut slik at det er lett og finne fram på spillet og at det er estetisk fint å se på. 

Akseptansekriterier for design:
- Når spilleren går inn i selve hovedmenyen, vil menyen være oversiktelig for at spilleren skal vite hvor han skal trykke. I tillegg er designet relevant for spillet hvor vi har brukt passende farger slik at layouten ser ryddig og fin ut. Når du går inn på selve spillet er selve bakgrunnen mørk men tiles-ene er lilla. Dette gjør at de forskjellige banene skiller seg ut og ikke blender inn med bakgrunnen. Karakteren man spiller er også i en farge som skiller seg ut, slik at det er lett og ha kontroll på hvor spilleren befinner seg. I spillet er det mye man kan gjøre men selve designet holdes enkelt slik at det ikke blir en rotete layout.  

Arbeidsoppgave:
- For å få til dette designet har vi tegnet forskjellige pixel tegninger som passer til tema. I tillegg har vi brukt farger som skiller seg ut men som samtidig ikke blir for mye igjen. Designene er enkle men samtidig spennende nok til å fange oppmerksomhet. Vi har også brukt en mørk hoved-bakgrunn og dermed brukt forskjellige farger som lyser opp spillet til resten av tiles og elementer. 