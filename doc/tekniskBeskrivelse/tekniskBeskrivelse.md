# Generell beskrivelse av prosjektet

Prosjektet er et spill hvor spillere kan navigere gjennom ulike nivåer, samle gjenstander, unngå hindringer samt plukke opp powerups.

# Arkitekturen til spillet

## Hovedkomponenter

- GameModel: Dette er kjernen i spillet hvor den meste av spillogikken håndteres. Klassen inneholder metoder for å initialisere spillet, oppdatere spilletilstand, og lagre/laste spilldata. Det er koblinger til mange andre klasser som Player, Room, Collectible, og Obstacle, som alle sammen representerer forskjellige aspekter og objekter i spillet.

- Player: Representerer spilleren i spillet. Den inneholder metoder for å håndtere bevegelse, interaksjon, og samhandling med andre objekter i spillet, som for eksempel oppsamling av gjenstander og powerups.

- Room og RoomLoader: Spillet er delt inn i forskjellige 'rom', der nivået gradvis blir vanskeligere. Klassen laster inn og administrerer elementene i hvert rom, inkludert plassering av hindringer, gjenstander og collectibles. Dette gjøres ved at klassen leser inn et bilde, og ved hjelp av fargekoder bestemmer hvor disse skal plasseres.

- GameController: Håndterer tastetrykk direkte. Den oversetter brukerinput til konkrete handlinger i spillet, som f.eks. å styre spillerens bevegelser og utføre hopping og dashing. Klassen bruker en timer for å jevnlig oppdatere spilltilstanden og håndtere endringer som pause av spillet.

- GameView: Fungerer som bindeleddet mellom spillets interne logikk og det brukeren faktisk ser og interagerer med på skjermen.
