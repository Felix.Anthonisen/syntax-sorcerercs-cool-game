Roller:

- (CEO) Prosjektleder --> Johanne
- (CDO) Admininstrerende designdirektør --> Cathrine
- Test-ansvarlig --> Malin
- Referansegruppe --> Johanne
- (CTO) Hovedansvar for kode --> Felix
- PR (løpende samtale med gruppeleder) --> Mathias
- Programmetodikk-leder (agile, kanban leder) --> Mathias
- Utviklere --> alle.

Hvorfor vi har valgt disse rollene:

- CEO -- > For å holde gruppen i sjakk. Være inkluderende til forslag og ideer. Passe på at alle i gruppa bidrar og føler seg sett.
- CDO --> Trenger å ha noen som er kunstnerisk og kreativ pga. spillet sin layout.
- Test-ansvarlig --> Trenger å se teste at koden faktisk fungerer som den skal ved å prøve ut ulike tester.
- Referansegruppe --> For å ha kontroll på oppgaver/møter og det som blir sagt.
- CTO --> Viktig med hovedansvar for å kunne ha overblikk over kode som har blitt skrevet for å unngå eventuelle feil som kan oppstå.
- PR --> Videreføre informasjon til gruppeleder om hvordan prosjektet går.
- Programmetodikk-leder --> For å ha system på prosjektet, at ting blir gjort i riktig rekkefølge osv.
- Utviklere --> alle skal kode for å få et ferdig produkt som alle har bidratt med.
