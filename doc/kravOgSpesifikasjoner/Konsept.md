Konsept
•	Plattformspill - liggende (4x3) - 960 x 720 pixel. 
•	En spillfigur som kan styres. 
•	Spillet er inspirert av Celeste. 
•	Gridbased spill med en enkeltperson som kan styres —> med piltaster og WASD taster.
•	Skal ha en dash funksjon og hoppe funksjon.
•	Collecter «en ting» for å få power ups (hoppe høyere, dobbel dash, anti-gravitation). 
•	Plattformer som beveger seg, hinder, forskjellige nivå/baner. Lik størrelse på blokkene. Klatre på vegger. 
•	Checkpoints (eventuelle snakkebobler fra figurer). 
•	Verden er større enn skjermen du er i. 
•	Collectables.