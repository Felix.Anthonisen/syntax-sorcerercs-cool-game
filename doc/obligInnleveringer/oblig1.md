# Rapport – innlevering 1

## Team: _Syntax sourceres_ – _Malin, Felix, Cathrine, Mathias, Johanne_...
Spill konsept:
• Plattformspill - liggende (4x3) - 960 x 720 pixel.
• En spillfigur som kan styres.
• Spillet er inspirert av Celeste.
• Gridbased spill med en enkeltperson som kan styres —> med piltaster og WASD taster.
• Skal ha en dash funksjon og hoppe funksjon.
• Collecter «en ting» for å få power ups (for eksempel å hoppe høyere i en gitt periode, dobbel dash, invisibility, anti gravitation).
• Plattformer som beveger seg, hinder, forskjellige nivå/baner. Lik størrelse på blokkene. Klatre på vegger.
• Checkpoints (eventuelle snakkebobler fra figurer).
• Verden er større enn skjermen du er i.
• Collectables.

## Prosjektmetodikk:

- Prosjektmetodikkene vi har gått for er Agile og Kanbas.

- Agile-metodikken legger vekt på kortsiktige mål, fleksibilitet og samarbeid i et team. Den er egnet for prosjekter der kravene endrer seg hyppig, og fokuserer på iterativ utvikling og rask tilpasning til endringer. Metodikken går ut på at man skal gå ut ifra korte sykluser hvor spillet skal endre seg mye, noe som gjør at mål, problem og annet vil oppstå jevnlig. Dette gjør det også enklere for personer å jobbe i team, ettersom det er mindre oppgaver som må gjøres, og man kan også jobbe parallelt med andre på oppgavene.
Agile er også en god metodikk for oss med tanke på at vi ikke har hatt klare mål fra starten av, og dermed er det enklere å oppdatere og utvikle mål underveis i arbeidet. Agile metodikken

- Kanban-metodikken er basert på visuell styring av arbeidsflyt, med fokus på jevn flyt av oppgaver og begrensning av arbeidsmengde for å unngå overbelastning. Den passer godt for prosjekter med kontinuerlige, repetetive oppgaver. 
Denne typen metodikk gjør også at vi får bedre oversikt over hva som må gjøres, hva folk holder på med, og hva som er fullført. Dette gjøres gjennom å ha 
et issue-board eller "Kanbanbrett" hvor vi legger inn problemer, ting som må gjøres, osv. og oppdaterer dette brettet etter hvert som ting blir gjort, eller ting settes igang. Dette gjør også at vi begrenser oss ned til mindre ting vi arbeider med, istedenfor å gjøre mange store oppgaver på en gang. Den er også lett tilpasselig, som gjør at vi kan tilpasse den slik vi ønsker, slik at den blir mest mulig oversiktlig for oss.

- Begge metodikkene er nyttige fordi de kan øke produktiviteten, forbedre kvaliteten på arbeid, fremme samarbeid og tilpasningsevne, og minske sløsing av ressurser.

## Roller:

- (CEO) Prosjektleder --> Johanne
- (CDO) Admininstrerende designdirektør --> Cathrine
- Test-ansvarlig --> Malin
- Referansegruppe --> Johanne
- (CTO) Hovedansvar for kode --> Felix
- PR (løpende samtale med gruppeleder) --> Mathias
- Programmetodikk - leder (agile, kanban leder) --> Mathias
- Utviklere --> alle.

## Fast møtetid / arbeidstid (utenom frivillig)

- Torsdag: 11.00 - 16.00

## Brukerhistorie:

### Spillere som skal spille:
* Barn og ungdom
* Uerfarne spillere
* Erfarne spillere

### Målgruppe
Spillet vårt er mest rettet mot ungdom, men det er også fokus på at andre folk skal ha en god opplevelse også. 
Spillet er laget på en måte som gjør at spillerne kan bestemme litt selv hvor intens spillopplevelsen skal bli, 
noe som gjør at spillet er spillbart for flere grupper. 

### Brukerens opplevelse 
Det er et utforskende spill, så det kan være spennende å finne nye og eller skjulte baner.
Det er mulig å samle mynter/collectables, og powerups som gjør at det kan bli enklere å fullføre kartene, 
eller som gir det fordeler for å fullføre kartet fortere. 
Vi gir spillerne en form for mestring for at det blir vanskeligere og vanskeligere, noe som gjør at spilleren 
må jobbe en del for å klare kartene etter hvert som man kommer lenger ut i spillet. 
Man kan for eksempel lære seg kombinasjoner av tastetrykk for å kombinere ting som hopping og dashing, noe som vil gjøre at 
man klarer kartene.
Brukeren opplever en fargekombinasjon som framstiller gode ting. Fargene vi har valgt er positivt framstilte farger, og er 
noe som gir brukeren dopamin når hen spiller.
For de mindre erfarne spillerne er det mulig å øve seg på kombinasjoner og bevegelse på de enklere kartene, og hvordan de skal bruke 
powerups på en positiv måte. Det er også slik at istedenfor at spilleren må starte fra start dersom hen dør, så respawner spilleren 
på samme kart. 
For de mer erfarne spillerne, har vi gjort de siste kartene en god del mer utfordrene som gjør at de får testet evnene sine
underveis i spillet. Dette gjør også at de kan få en slags overraskelse, ettersom de kan føle at de første kartene er enkle i
forhold til de siste kartene.

## A5:
**Det som har fungert bra:**

- Satt oss godt inn i oppgaven. Fått delt ut roller som passer godt til alle. Enighet i prosjektet, alle liker ideen og har kommet med forslag og ideer for spill og layout.

Ikke fungert helt som forventet:

- Tekniske problemer med box2D. Litt vanskelig og sette seg inn i GIT og hele samspillet med pull-push-commit med flere på samme prosjekt.

**Ikke fungert i det hele tatt:**

- Ingen ting vi kommer på enda, har ikke fått jobbet nok med prosjektet til å vite om noe ikke har gått bra.

Vi har kommet satt oss inn og kommet greit igang med oppgaven. Vi har også fått tildelt roller slik at alle bidrar og har fått være med på idee-myldrigen. Nå er selve gridet oppe, og en figur som kan hoppe + gå fram og tibake. Videre i prosjektet skal vi fortsette på figuren og få mer av spillbrettet ferdig.
