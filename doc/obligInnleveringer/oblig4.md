# Rapport – innlevering 4

**Team: syntaxSorcerers**
 
## Prosjektrapport
### Retrospektiv
Rollene vi utga i starten av prosjektet har fungert godt utover hele prosjektet. Vi har tatt rollene på alvor og jobbet med det vi skal jobbe med løpende. Har merket at noen av rollene har hatt større arbeidsmengde enn andre, men vi har fordelt det deretter. For eksempel har testansvarlig fått litt hjelp med tester ettersom dette var en større, og mer krevende oppgave enn vi trodde. Det har generelt vært mye enighet i gruppen om hvordan både spillet skal være, layout og arbeidsoppgaver. Samarbeidet har fungert godt, og vi har ikke hatt noen problemer som gruppe generelt. Vi har delt ut forskjellige arbeidsoppgaver til alle, i tillegg til at det har blitt gjort en del pair-koding og diskutering på møtene. Vi sitter generelt mye sammen på skolen, og har derfor fått jobbet mye sammen på prosjektet. Vi snakker godt sammen, og er det uenigheter så diskuterer vi om disse, og blir enige. Vi har alle vært med på samme idé fra start, og vi er veldig fornøyd med resultatet av spillet. 

Det å bruke Git som et samarbeidsverktøy har vært både positivit og negativt. I starten slet vi litt med dette, og måtte sette oss godt inn i hvordan man bruker det. Etter flere conflicts ble vi bedre på å bruke Git og hvis det har oppstått problemer i ettertid, har vi fått fikset dette. Det er nå mye mindre komplikasjoner enn det det var i starten, og det vi har fått tilbake fra dette er mer erfaring på bruken av Git til eventuelle fremtidige prosjekter. 

Når det kommer til selve prosjektmetodikken har vi blitt flinkere til å bruke issue-boardet. Vi brukte det ikke aktivt i starten, og dette gjorde det vanskeligere å ha oversikt over arbeidsoppgaver for prosjektet. Men vi har nå vært flinke til å legge inn issues, og jobbet ut ifra dette. Det har vært til stor hjelp for prosjektet, og det har gjort det lettere å holde prosjektet ryddig, i tillegg til at vi har fått en god oversikt over hva vi har fått gjort, hva vi mangler og eventuelle issues som har oppstått. Det har vært en veldig ryddig måte å bruke issue-board, spesielt når vi er flere som skal jobbe på prosjektet samtidig. 
Vi er fortsatt veldig fornøyd med Agile-metodikken, ettersom dette har gjort at vi har hatt mindre mål etter hvert som vi har jobbet med prosjektet. Dette gir oss en bedre følelse av progression gjennom prosjektet, og det gjør det enklere å komme seg videre i prosjektet. 

Kommunikasjonen har fungert godt gjennom prosjektet. Det har ikke alltid vært mulig å samle alle til møtene, men da har vi brukt Discord og SnapChat aktivt, for å informere om det som har vært viktig for prosjektet. Som nevnt tidligere sitter vi mye sammen på skolen og da får vi jobbet godt og diskutert mye. Vi er gode venner, og det er derfor vært god kommunikasjon mellom oss. Vi har hatt det veldig kjekt underveis i tillegg til seriøs jobbing for å få til det ferdige produktet. 

Prosjektet i seg selv har gått veldig bra. Vi har vært flinke til å jobbe godt underveis, og fordelt arbeidet jevnt utover de forskjellige månedene. Det har vært mange gode ideer, som har gjort at vi har endt opp med et spill vi kan si oss fornøyd med. Alle har bidratt masse underveis med både koding, referanser, design osv. Om vi skulle ha gjort noe annerledes fra start, så burde vi nok ha startet tidligere med issue-boardet. Vi burde ha tatt det i bruk på en mer oversiktelig måte. Det ville ha vært med på å lage en klarere oversikt over hva som skal gjøres, hvem som gjør hva, hva som er blitt gjort, hva som jobbes med nå osv. tidligere i prosessen. 


## Krav og spesifikasjon
**Kravene vi har hatt gjennom prosjektet:**

- Vise et spillbrett
- Vise karakter på spillbrett
- Kunne flytte karakteren
- Interaction mellom karakter og obstacles
- Hinder
- Spilleren dør dersom den treffer hinder
- Et mål for hele spillet og hvert level
- Start-skjerm, med informasjon om spillet og kontrollene
- Mulighet for å plukke opp powerups for å gjøre banene enklere
- Mulighet for å plukke opp collectables

Alle kravene vi har satt oss, har vi klart å gjennommføre, i tillegg til også å legge til ekstra funksjoner utenom dette. Alle kravene vi har satt oss, har vært for å gjøre spillet mest mulig progressivt for den som skal spille det, slik at spilleren føler at hen kommer lenger ut i spillet etterhvert som leveler blir fullført, og collectables samles.

Kravene vi satt oss i starten (MVP) var simple nok for at vi enkelt kunne gjennomføre dette på en god måte. Dette gjorde også at det ble enklere å skulle utvide dette senere, som for eksempel å legge til flere maps, legge inn flere funksjoner som viser progresjon for spillet osv.

Nå som spillet er ferdig, så har vi ferdigstilt og oppgradert kravene slik at de er verdig et komplett prosjekt, og vi ser at kravene vi satt for spillet i starten var riktig for hvordan vi ønsket at spillet skulle ende opp. 

**Bugs vi vet om:**

Spilleren kan til tider stå på siden av veggen, og dette gjør også at spilleren på noen steder kan doubledashe. Dette skjer veldig sjeldent, og er i realiteten ikke en stor issue for et ferdigstilt prosjekt.

## Produkt og kode
### Dette har vi fikset siden sist
- vi har oppdatert brukerhistorier og skrevet det mer utfyllende ( i en egen brukerhistorie fil)
- funnet ut hvorfor spillet ikke kjørte via maven og endret (.jar er filnavn sensitiv)
- laget et klassediagram som beskriver hvordan arkitekturen til spillet er bygd opp
- lagt til interfaces og skapt mer abstraksjon og access control ved å kun gi tilgang til riktige metoder. Eks. to interfaces til GameModel fordi mange metoder ikke er nødvendig å ha tilgang til fra f.eks view.
- endret kode så vi får riktig testcoverage og laget mange tester 
- lagd flere edgecase tester, Eks. hva skjer hvis man får unexpected input?
- endret factory slik at det ikke skal måtte endres for å legge til nye items.
