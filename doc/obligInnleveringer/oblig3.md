# Rapport – innlevering 3

**Team: syntaxSorcerers**

## Prosjektrapport
### Trenger dere å oppdatere hvem som er teamlead eller kundekontakt?
Slik det er per nå, så føler vi ikke at vi trenger å oppdater hvem som er teamleder eller kontakt. Rollene vi har i teamet fungerer veldig fint, og vi har generelt en god kommunikasjon mellom alle som gjør at de forskjellige rollene fungerer veldig fint slik de er nå. 
Alle tar ansvar for den/de rollene de har, og vi har vært flinke til å delegere rundt dersom det er noe vi føler mangler.

### Trenger dere andre roller? Skriv ned noen linjer om hva de ulike rollene faktisk innebærer for dere.
Per nå, føler vi ikke at vi trenger noen flere roller enn det vi har nå. De rollene vi bruker nå fungerer veldig fint, og gjør at vi har en god oversikt på prosjektet vårt, hva som må gjøres og hva som er gjort, og kommunikasjonen mellom rollene fungerer også veldig bra.

- (CEO) Prosjektleder - Denne rollen innebærer at lederen har kontroll over prosjektet og hva som må gjøres. Prosjektlederen kan også delegere oppgaver til andre slik at alle har noe å gjøre til enhver tid, og ikke må lete etter oppgaver å gjøre. Prosjektlederen er også der for å ta avgjørelser angående prosjektet, dersom det er uenigheter innad i gruppen. For eksmepel når det kommer til hva som skal være med og ikke, er det ofte prosjektlederen som tar den siste avgjørelsen. 
- (CDO) Admininstrerende designdirektør - Denne rollen innebærer å ta kontroll over designet på prosjektet. Dette går ut på å finne/lage ulike sprites, bakgrunner, tiles, meny, kart osv. Designdirektøren har siste ord når det kommer til designet på spillet, og hvordan ting skal se ut. 
- Test-ansvarlig - Denne rollen innebærer å holde kontroll på hva som må testes og ikke. Den ansvarlige skal også ha kontroll på test-coverage, og hvor mye coverage vi har over prosjektet vårt når det kommer til testene. Hen er også ansvarlig for å sjekke at testene skrives riktig, og at testene faktisk fungerer. 
- (CTO) Hovedansvar for kode - Denne rollen innebærer å holde kontroll på at koden er god, oversiktlig og at den er effektiv. Hen er også ansvarlig for at vi opprettholder riktig måte å kode på og, at vi holder oss til den strategien vi ønsker. 
- PR (løpende samtale med gruppeleder) - Denne rollen innebærer enkelt å greit å holde kontakt med gruppeleder underveis i prosjektet
- Programmetodikk - leder (agile, kanban) - Denne rollen innebærer å holde kontroll på at vi følger prosjektmetodikken, og at vi prøver å gjøre dette på en god og effektiv måte for prosjektet vårt. Dette innebærer for eksmepel å sørge for at vi holder oss til korte og presise mål, at vi oppdaterer issue-board og holder det riktig osv. 
- Utviklere - Denne rollen innebærer enkelt og greit å skrive kode, og lage spillet. 

### Er det noen erfaringer enten team-messig eller mtp prosjektmetodikk som er verdt å nevne? Synes teamet at de valgene dere har tatt er gode? Hvis ikke, hva kan dere gjøre annerledes for å forbedre måten teamet fungerer på?
Når det kommer til prosjektmetodikk så har vi ikke vært så gode på å utnytte Kanban. Vi har hatt et issue-board hele veien, men vi har ikke vært så gode til å faktisk utnytte dette til det fulle, ved å oppdatere det jevnlig. Vi kunne skrevet skikkelig issues med god beskrivelse, og hatt flere kategorier (slik vi har det nå), slik at det er enklere å holde oversikt over hva som skjer i prosjektet, og hva som må gjøres. 

Generelt sett synes vi valgene vi har tatt har vært gode, vi har kommet med ideer som vi i starten har følt kunne vært bra, men etter hvert så har vi funnet ut at det kanskje ikke funker så bra. Da har vi funnet løsninger og andre alternativer kjapt, slik at vi kommer oss videre. 

Det vi kan gjøre for å forbedre måten teamet fungerer på er at vi kan være enda flinkere på å kommunisere underveis i arbeidet, spesielt mtp issue-board, slik at alle har oversikt over hvem som driver med hva, hva som må gjøres osv. Dette vil være en stor fordel, ettersom det vil øke effektiviteten for arbeidet generelt. 

Valget vårt når det kommer til Agile-metodikken er vi veldig fornøyd med. Dette har gjort at vi har følt mye mer på progresjon etter hvert som vi har nådd de ulike små målene vi har satt oss. Det har også vært enklere på å holde kontroll på hva vi skal fokusere på, og vi har enklere oversikt over hva som skal prioriteres. Vi syens vi har vært gode på å holde oss til en slik framgang, og tenker at vi bare bør fortsette slik nå den siste perioden.

### Hvordan er gruppedynamikken? Er det uenigheter som bør løses?
Gruppedynamikken er veldig god. Vi var allerede gode venner før prosjektet og er godt vant med å samarbeide. Prosjektet har bare gjort oss enda bedre kjent og enda bedre venner. Det er fint å ha en gruppe som kan samarbeide godt og samtidig slå av en spøk her og der uten at alt er så blodseriøst. Det hjelper ofte på motivasjonen til å jobbe sammen, at det ikke bare er kjipt når man er stuck på et problem.

Det er ikke nødvendigvis noen uenigheter som bør løses, men et problem kan være at det til tider kan bli litt mye tull, og litt lite fokus på oppgaven ettersom vi er så komfortable med hverandre. Vi er gode på å ta oss sammen om vi merker det blir mye, men det har skjedd en del ganger at fokuset forsvinner litt for lenge, og dette er nok noe vi bør bli bedre på fremover.

### Hvordan fungerer kommunikasjonen for dere?
Kommunikasjonen funker fint, vi bruker både discord og snapchat som hovedkommunikasjonskanaler. Discord kanalen brukes til å kommusiere mer seriøse hendvendelser angående prosjektet, mens snapchat er en lavterskel kommunikasjonskanal. Vi er også blitt bedre på å oppdatere issue-boardet som gjør at vi har bedre oversikt over prosjektet, i tillegg til at vi oppdaterer på Discord dersom vi gjør noe drastiske endringer, eller om det er noe vi føler de andre trenger å vite angående prosjektet. 

Det å ha en lavterskel og en seriøs kommunikasjonskanal har fungert ganske bra for oss, ettersom dette gjør at det blir enklere for oss å splitte de ulike meldingene, og holde alt det seriøse separert fra ting som gjerne fort kan bli litt useriøst. 


## Retrospektiv:
Er fortsatt litt skjevfordelt mtp commits, men dette vil endre seg mer og mer etter hvert som vi nå begynner å få inn en del tester. Det er også fortsatt gjort en del parkoding som gjør at noen får flere commits enn andre, og noen gjør mer arbeid mellom hver commit. 
Det vi gjør bra nå er at vi holder hverandre relativt godt oppdatert på hva som er pushet og når vi pusher det, slik at folk får pullet nye oppdateringer i koden så fort som mulig. Vi er også blitt bedre på å fordele oppgaver til alle, og være mer spesifisert på hva som må gjøres, men vi kan fortsatt bli bedre på dette.

Forbedringspunkter: 
- Skrive mer detaljert hva som må gjøres på issue-board (evt forbedre issue-board)
- Holde oss til ett språk når vi skriver commit-meldinger og issues
- Fordele oppgaver enda jevnere, ettersom det er litt spredning i commits
- Oppdatere issue-board mer jevnlig, så det er enklere å ha kontroll på hvem som jobber med hva

## Krav og spesifikasjon
Vi er kommet godt forbi MVP'en vår nå, det eneste vi mangler for å gjøre det komplett er at vi legger inn et mål som connecter kart 1, 2, 3 osv.

**KRAV: La spilleren gå mellom kartene ved bruk av portal / dør**
- **Brukerhistorier** - Det å kunne gå mellom kartene både fram og tilbake, gjør at spillere som ikke er så erfarne har mulighet til å gå tilbake til tidligere kart dersom de ønsker å utforske mer, eller ønsker å finne ut av ting de ikke har tenkt over før senere. 
Det er også en fordel for mer erfarne spillere å ha flere kart å kunne gå gjennom ettersom dette vil kunne gi dem en følelse av at de kommer seg fort gjennom spillet (også kalt speedrunning). Dersom vi bare har ett stort kart, hvor det gjerne bare er litt collectables her og der, kan det bli vanskelig å gi den samme følelsen av mestring, både for erfarne og uerfarne spillere. 

- **Akseptansekriterier** - For at dette skal fungere er det viktig at spilleren skal kunne gå mellom kartene. Det er viktig at når spilleren går til neste kart, så skal karakteren spawne på riktig posisjon. Dersom spilleren går tilbake til forrige kart, så skal spilleren spawne der den originalt gikk inn i portalen/døren. Dette er noe vi må ha mellom alle kart, slik at det er mulig å komme seg gjennom hele spillet. Dørene i de første kartene vil være en god del enklere å komme seg til, enn å komme seg til dørene/portalene på de siste kartene. 

- **Arbeidsoppgaver** - Arbeidsoppgavene for dette vil være: lage maps, lage dørene/portalene mellom de ulike kartene, skape en interaksjon mellom karakteren og portalen/døren, oppdatere spawnposisjon for karakteren ut ifra om den kommer fra et tidligere eller et senere level og oppdatere hvilket kart som tegnes ut ifra hvilket level karakteren faktisk er på. 

**Bugs vi vet om per nå:**
- Spilleren ender sporadisk opp med å kunne stå på veggen 
- Spilleren kan noen ganger dashe to ganger, dette problemet vil automatisk løses når vi løser problemet over
- Tegning av dashDust texturen blir ikke tegnet ut riktig på skjermen ut ifra hvilken retning karakteren dasher.

## Produkt og kode
Dette er det vi har fikset siden sist: 
- Skrevet en del tester (for tiden på omtrent 50% test coverage)
- Fått spilleren til å interakte med objekter på kartet (dør når den treffer spikes)
- Laget respawn metode for spilleren slik at spilleren respawner når han dør
- Skrevet JavaDocs for metoder i de ulike klassene
- Fikset noen bugs i karakterens animasjoner
- Implementert en room-loader som gjør at vi kan laste inn kart fra egentegnede bilder
- Laget en controls page
- Fikset bakgrunnsmusikk og lyder når spilleren gjør ting
- Refaktorert filsystemet slik at det er mer oversiktlig
- Fikset forskjellige bugs i spillet@