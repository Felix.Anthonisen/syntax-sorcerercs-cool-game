# Rapport – innlevering 2

**Team: syntaxSorcerers**

## Prosjektrapport

- Rollene i teamet fungerer fint. Folk gjør det de skal.
- Føler på ikke på dette tidspunktet at det er behov for flere roller.
- Et agile arbeidsflyt fungerer veldig bra. Det gjør det lett å innføre ny funksjonalitet og arbeide effektivt.
-
- Teamet har god kommunikasjon både på nett og på skolen.

### Retrospektiv:

- Er litt skjev fordeling mtp. commits, dette skyldes at vi har gjort mye parkoding og en del av arbeidet med visuelle assets ikke nødvendigvis blir pushet opp av personen som har laget det.
- Forbedringspunkter:
  - Ha enda bedre kommunikasjon mtp. hvem som er ansvarlig for ulike arbeidsoppgaver.
  - Bedre plan på hvordan ny funksjonalitet skal implementeres
  - Bruke issue board aktivt

## Krav og spesifikasjon

- MVP er nå i stor grad oppnådd. All bevegelse er nå på plass og kollisjoner fungerer som det skal.
- Til nå har mye av fokuset vært vektlagt på å få en robust engine, slik at det blir enklere med videre utvikling av spillet.
- Fokus videre er å lage rom til spillet. For å gjøre dette trenger vi ulike tile typer og bygge leveler.
- Tester har blitt utelatt frem til nå fordi mesteparten av koden ikke har blitt satt i sten før nå. Dette skal prioriteres videre for å forsikre en robust kodebase.
- Bugs: spiller setter seg sporadisk fast i vegg når klatreknapp slippes. Sporadiske fps-drops, må finne ut hva dette skyldes.

## Produkt og kode

Dette har vi fikset siden sist:

- Inkorporert box2d i engine slik at den håndterer akselerasjon og kollisjoner mellom bodies
- Pauseskjerm og main menu screen
- Fått inn rutenettet og renderer dette
- Death counter
- All bevegelse er på plass (dash, climb, hopp, løp osv.)
- Animert spilleren
- Laget nødvendige enum klasser
- Støtte for drepe og respawne spiller og restarte spill
- Fullført MVP
